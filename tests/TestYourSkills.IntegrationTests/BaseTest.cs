﻿using Bogus;
using TestYourSkills.IntegrationTests.FakeData;

namespace TestYourSkills.IntegrationTests;

public class BaseTest
{
    protected FakeTopic FakeTopics { get; private set; }
    protected Faker Faker { get; private set; }
    
    [SetUp]
    public void SetupBeforeEachTest()
    {
        const int seed = 1234;

        Randomizer.Seed = new Random(seed);

        Faker = new Faker("uk");
        FakeTopics = new FakeTopic(seed);
    }
}