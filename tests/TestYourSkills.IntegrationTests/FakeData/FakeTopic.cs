﻿using Bogus;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.IntegrationTests.FakeData;

public class FakeTopic
{
    public FakeTopic(int seed)
    {
        WithoutQuestions = WithoutQuestions.UseSeed(seed);
    }

    public Faker<Topic> WithoutQuestions { get; private set; }
        = new Faker<Topic>("uk")
            .CustomInstantiator(f => new Topic
            {
                Name = Guid.NewGuid().ToString()
            });

    public Faker<Topic> WithQuestionsWithoutQuestionAnswers { get; private set; }
        = new Faker<Topic>("uk")
            .CustomInstantiator(f => new Topic
            {
                Name = Guid.NewGuid().ToString(),
                Questions = new List<Question>()
                {
                    new()
                    {
                        Title = Guid.NewGuid().ToString(),
                    },
                    new()
                    {
                        Title = Guid.NewGuid().ToString(),
                        Details = Guid.NewGuid().ToString()
                    }
                }
            });

    public Faker<Topic> WithQuestionsWithQuestionAnswers { get; private set; }
        = new Faker<Topic>("uk")
            .CustomInstantiator(f => new Topic
            {
                Name = Guid.NewGuid().ToString(),
                Questions = new List<Question>()
                {
                    new()
                    {
                        Title = Guid.NewGuid().ToString(),
                        MultiAnswers = true,
                        QuestionAnswers = new List<QuestionAnswer>()
                        {
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                                CorrectAnswer = true
                            },
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                            },
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                                CorrectAnswer = true
                            }
                        }
                    },
                    new()
                    {
                        Title = Guid.NewGuid().ToString(),
                        Details = Guid.NewGuid().ToString(),
                        QuestionAnswers = new List<QuestionAnswer>()
                        {
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                            },
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                            },
                            new()
                            {
                                Message = Guid.NewGuid().ToString(),
                                CorrectAnswer = true
                            }
                        }
                    }
                }
            });
}