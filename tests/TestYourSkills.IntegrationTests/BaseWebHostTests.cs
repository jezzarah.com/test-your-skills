﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TestYourSkills.Core.Models.Identity;
using TestYourSkills.Data.Data;
using TestYourSkills.Data.Identity;
using TestYourSkills.Web.Extensions;

namespace TestYourSkills.IntegrationTests;

public class BaseWebHostTests : BaseTest
{
    protected TestServer _server;
    private IHost _host;
    private IConfiguration _configuration;
    private string _databasePath;
    
    [SetUp]
    public async Task BaseWebHostSetup()
    {
        var tempDir = Path.GetTempPath();
        _databasePath = Path.Combine(tempDir, "test.db");
        
        // Создаем конфигурацию вручную
        _configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        
        // Создаем тестовый сервер
        var hostBuilder = new HostBuilder()
            .ConfigureWebHost(webHost =>
            {
                webHost.UseStartup<TestStartup>()
                    .UseTestServer()
                    .UseEnvironment("Testing")
                    .UseConfiguration(_configuration);
            })
            .ConfigureServices(services =>
            {
                services.AddControllersWithViews();
                services.AddApplicationServices(_configuration)
                    .AddIdentityServices(_configuration);
                
                services.AddDbContext<DataContext>(options =>
                    options.UseSqlite($"Data Source={_databasePath}"));
                    //options.UseSqlite("Data Source=:memory:")); // In some case data still here after restart, maybe cashed
    
                services.AddDbContext<IdentityContext>(options =>
                    options.UseSqlite("Data Source=:memory:"));
                
                services.AddScoped<UserManager<AppUser>>();
    
                using var scope = services.BuildServiceProvider().CreateScope();
                var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                var identityContext = scope.ServiceProvider.GetRequiredService<IdentityContext>();
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                dataContext.Database.Migrate();
                DataContextSeed.SeedDefaultQuestionsAsync(dataContext, _configuration).Wait();
    
                identityContext.Database.Migrate();
                IdentityContextSeed.SeedUsersAsync(userManager, roleManager, _configuration).Wait();
            });
    
        var host = await hostBuilder.StartAsync();
        _host = host;
        _server = host.GetTestServer();
    }
    
    [TearDown]
    public async Task BaseWebHostCleanUp()
    {
        await _host.StopAsync();
        _host?.Dispose();
        
        // Удаление временной базы данных
        File.Delete(_databasePath);
    }
}