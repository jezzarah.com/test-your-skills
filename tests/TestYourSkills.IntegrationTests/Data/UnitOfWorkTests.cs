﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.IntegrationTests.Data;

public class UnitOfWorkTests : BaseWebHostTests
{
    [Test]
    public async Task AddNewTopic_WithoutQuestions()
    {
        // Arrange
        var unitOfWork = _server.Services.GetRequiredService<IUnitOfWork>();
        var newTopic = FakeTopics.WithoutQuestions.Generate(1).First();

        // Act
        unitOfWork.Repository<Topic>().Add(newTopic);
        await unitOfWork.Complete();

        // Assert
        Assert.NotNull(newTopic);
        Assert.NotZero(newTopic.Id);

        // check db
        var addedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);
        addedTopic.Should().NotBeNull();
        addedTopic!.Name.Should().Be(newTopic.Name);
    }

    [Test]
    public async Task AddNewTopic_WithQuestions_WithoutQuestionAnswers()
    {
        // Arrange
        var unitOfWork = _server.Services.GetRequiredService<IUnitOfWork>();
        var newTopic = FakeTopics.WithQuestionsWithoutQuestionAnswers.Generate(1).First();

        // Act
        unitOfWork.Repository<Topic>().Add(newTopic);
        await unitOfWork.Complete();

        // Assert
        Assert.NotNull(newTopic);
        Assert.NotZero(newTopic.Id);

        // check db
        var addedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);
        addedTopic.Should().NotBeNull();
        addedTopic!.Name.Should().Be(newTopic.Name);
        addedTopic.Questions.Should().NotBeNull();

        foreach (var question in addedTopic.Questions!)
        {
            var existingQuestion = await unitOfWork.Repository<Question>().GetByIdAsync(question.Id);
            existingQuestion.Should().NotBeNull();
            existingQuestion!.Title.Should().Be(question.Title);
            existingQuestion.Details.Should().Be(question.Details);
            existingQuestion.MultiAnswers.Should().Be(question.MultiAnswers);
            existingQuestion.TopicId.Should().Be(question.TopicId);
        }
    }

    [Test]
    public async Task AddNewTopic_WithQuestions_WithQuestionAnswers()
    {
        // Arrange
        var unitOfWork = _server.Services.GetRequiredService<IUnitOfWork>();
        var newTopic = FakeTopics.WithQuestionsWithQuestionAnswers.Generate(1).First();

        // Act
        unitOfWork.Repository<Topic>().Add(newTopic);
        await unitOfWork.Complete();

        // Assert
        Assert.NotNull(newTopic);
        Assert.NotZero(newTopic.Id);

        // check db
        var addedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);
        addedTopic.Should().NotBeNull();
        addedTopic!.Name.Should().Be(newTopic.Name);
        addedTopic.Questions.Should().NotBeNull();

        foreach (var question in addedTopic.Questions!)
        {
            var existingQuestion = await unitOfWork.Repository<Question>().GetByIdAsync(question.Id);
            existingQuestion.Should().NotBeNull();
            existingQuestion!.Title.Should().Be(question.Title);
            existingQuestion.Details.Should().Be(question.Details);
            existingQuestion.MultiAnswers.Should().Be(question.MultiAnswers);
            existingQuestion.TopicId.Should().Be(question.TopicId);
            existingQuestion.QuestionAnswers.Should().NotBeNull();

            foreach (var questionAnswer in existingQuestion.QuestionAnswers!)
            {
                var existingQuestionAnswer =
                    await unitOfWork.Repository<QuestionAnswer>().GetByIdAsync(questionAnswer.Id);
                existingQuestionAnswer.Should().NotBeNull();
                existingQuestionAnswer!.Message.Should().Be(questionAnswer.Message);
                existingQuestionAnswer.CorrectAnswer.Should().Be(questionAnswer.CorrectAnswer);
                existingQuestionAnswer.QuestionId.Should().Be(questionAnswer.QuestionId);
            }
        }
    }

    [Test]
    public async Task Update_Topic()
    {
        // Arrange
        var unitOfWork = _server.Services.GetRequiredService<IUnitOfWork>();
        var newTopic = FakeTopics.WithoutQuestions.Generate(1).First();
        var newTopicName = Guid.NewGuid().ToString();

        // Act
        unitOfWork.Repository<Topic>().Add(newTopic);
        await unitOfWork.Complete();

        var addedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);

        newTopic.Name = newTopicName;
        unitOfWork.Repository<Topic>().Update(newTopic);
        await unitOfWork.Complete();

        // Assert
        addedTopic.Should().NotBeNull();

        // check db
        var updatedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(addedTopic!.Id);
        updatedTopic.Should().NotBeNull();
        updatedTopic!.Name.Should().Be(newTopic.Name);
    }

    [Test]
    public async Task Delete_Topic()
    {
        // Arrange
        var unitOfWork = _server.Services.GetRequiredService<IUnitOfWork>();
        var newTopic = FakeTopics.WithoutQuestions.Generate(1).First();

        // Act
        unitOfWork.Repository<Topic>().Add(newTopic);
        await unitOfWork.Complete();

        var addedTopic = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);

        unitOfWork.Repository<Topic>().Delete(newTopic);
        await unitOfWork.Complete();

        // Assert
        addedTopic.Should().NotBeNull();

        // check db
        var exist = await unitOfWork.Repository<Topic>().GetByIdAsync(newTopic.Id);
        exist.Should().BeNull();
    }
}