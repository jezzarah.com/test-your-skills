﻿using System.Text.Json;
using FluentAssertions;
using Moq;
using StackExchange.Redis;
using TestYourSkills.Data.Services;

namespace TestYourSkills.IntegrationTests.Services;

public class CacheServiceTests
{
    private Mock<IConnectionMultiplexer> _redisMock;
    private Mock<IDatabase> _databaseMock;
    private CacheService _cacheService;

    [SetUp]
    public void Setup()
    {
        _databaseMock = new Mock<IDatabase>();
        _redisMock = new Mock<IConnectionMultiplexer>();
        _redisMock.Setup(r => r.GetDatabase(It.IsAny<int>(), It.IsAny<object>())).Returns(_databaseMock.Object);

        _cacheService = new CacheService(_redisMock.Object);
    }

    [Test]
    public async Task GetCacheAsync_ShouldReturnCachedData_WhenCacheKeyExists()
    {
        // Arrange
        var cacheKey = "test_key";
        var cachedData = new TestObject { Id = 1, Name = "Test" };
        var serializedData = JsonSerializer.Serialize(cachedData);
        _databaseMock.Setup(d => d.StringGetAsync(cacheKey, It.IsAny<CommandFlags>()))
            .ReturnsAsync(serializedData);

        // Act
        var result = await _cacheService.GetCacheAsync<TestObject>(cacheKey);

        // Assert
        result?.Id.Should().Be(cachedData.Id);
        result?.Name.Should().Be(cachedData.Name);
    }

    [Test]
    public async Task GetCacheAsync_ShouldReturnNull_WhenCacheKeyDoesNotExist()
    {
        // Arrange
        var cacheKey = "non_existing_key";
        _databaseMock.Setup(d => d.StringGetAsync(cacheKey, It.IsAny<CommandFlags>()))
            .ReturnsAsync(RedisValue.Null);

        // Act
        var result = await _cacheService.GetCacheAsync<TestObject>(cacheKey);

        // Assert
        result.Should().BeNull();
    }

    [Test]
    public async Task SetCacheAsync_ShouldStoreDataInCache_WithGivenTimeToLive()
    {
        // Arrange
        var cacheKey = "test_key";
        var data = new TestObject { Id = 1, Name = "Test" };
        var serializedData = JsonSerializer.Serialize(data);
        var timeToLive = TimeSpan.FromMinutes(10);
        _databaseMock.Setup(d =>
                d.StringSetAsync(cacheKey, serializedData, timeToLive,
                    It.IsAny<bool>(), It.IsAny<When>(), It.IsAny<CommandFlags>()))
            .ReturnsAsync(true);

        // Act
        var result = await _cacheService.SetCacheAsync(cacheKey, data, timeToLive);

        // Assert
        result.Should().BeTrue();
    }

    [Test]
    public async Task DeleteCacheAsync_ShouldDeleteCacheKey()
    {
        // Arrange
        var cacheKey = "test_key";
        _databaseMock.Setup(d => d.KeyDeleteAsync(cacheKey, It.IsAny<CommandFlags>()))
            .ReturnsAsync(true);

        // Act
        var result = await _cacheService.DeleteCacheAsync(cacheKey);

        // Assert
        result.Should().BeTrue();
    }

    private class TestObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}