﻿using FluentAssertions;
using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Services;

namespace TestYourSkills.IntegrationTests.Services;

public class RandomizerTests
{
    private IRandomizer _randomizer;
    
    [SetUp]
    public void Setup()
    {
        _randomizer = new Randomizer();
    }
    
    [Test]
    public async Task GenerateRandomIntAsync_GeneratesRandomIntegersWithinRange()
    {
        // Arrange
        int min = 1;
        int max = 100;
        int count = 10;

        // Act
        var result = await _randomizer.GenerateRandomIntAsync(min, max, count);

        // Assert
        result.Count().Should().Be(count);
        result.Should().OnlyContain(element => element >= min && element <= max);
        
        // the same
        Assert.AreEqual(count, result.Count());
        Assert.That(result, Is.All.InRange(min, max));
    }
    
    [Test]
    public async Task GetRandomElementsAsync_ReturnsRandomElements()
    {
        // Arrange
        var collection = Enumerable.Range(1, 10);
        var count = 5;

        // Act
        var result = await _randomizer.GetRandomElementsAsync(collection, count);

        // Assert
        result.Count().Should().Be(count);
        result.Should().BeSubsetOf(collection);
        
        // the same
        Assert.AreEqual(count, result.Count());
        Assert.That(result, Is.SubsetOf(collection));
    }

    [Test]
    public async Task GetRandomElementsAsync_ReturnsSameCollection_WhenCountEqualsCollectionSize()
    {
        // Arrange
        var collection = Enumerable.Range(1, 10);
        var count = collection.Count();

        // Act
        var result = await _randomizer.GetRandomElementsAsync(collection, count);

        // Assert
        result.Should().BeEquivalentTo(collection);
        
        // the same
        CollectionAssert.AreEquivalent(new HashSet<int>(collection), new HashSet<int>(result));
    }

    [Test]
    public async Task GetRandomElementsAsync_ReturnsEmptyCollection_WhenCountIsZero()
    {
        // Arrange
        var collection = Enumerable.Range(1, 10);
        var count = 0;

        // Act
        var result = await _randomizer.GetRandomElementsAsync(collection, count);

        // Assert
        result.Count().Should().Be(count);
        result.Should().BeEmpty();
        
        // the same
        Assert.AreEqual(count, result.Count());
        Assert.IsEmpty(result);
    }
}