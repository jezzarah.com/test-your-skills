using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TestYourSkills.Core.Models.Identity;
using TestYourSkills.Data.Data;
using TestYourSkills.Data.Identity;
using TestYourSkills.Web.Extensions;

var builder = WebApplication.CreateBuilder(args)
    .ApplicationSetup();
// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
builder.Services.AddApplicationServices(builder.Configuration)
    .AddIdentityServices(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

// app.MapControllerRoute(
//     name: "area",
//     pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "default",
    pattern: "{area=User}/{controller=Home}/{action=Index}/{id?}");

using var score = app.Services.CreateScope();
var services = score.ServiceProvider;
var dataContext = services.GetRequiredService<DataContext>();
var identityContext = services.GetRequiredService<IdentityContext>();
var userManager = services.GetRequiredService<UserManager<AppUser>>();
var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
try
{
    await dataContext.Database.MigrateAsync();
    await DataContextSeed.SeedDefaultQuestionsAsync(dataContext, builder.Configuration);
    
    await identityContext.Database.MigrateAsync();
    await IdentityContextSeed.SeedUsersAsync(userManager, roleManager, builder.Configuration);
}
catch (Exception exc)
{
    Console.WriteLine(exc);
    throw;
}

app.Run();