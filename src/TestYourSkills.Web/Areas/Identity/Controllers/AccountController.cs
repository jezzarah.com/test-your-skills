﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TestYourSkills.Core.Models.Identity;
using TestYourSkills.Core.ViewModels;

namespace TestYourSkills.Web.Areas.Identity.Controllers;

[AllowAnonymous]
[Area("Identity")]
public class AccountController : Controller
{
    private readonly UserManager<AppUser> _userManager;
    private readonly SignInManager<AppUser> _signInManager;

    public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
    {
        _userManager = userManager;
        _signInManager = signInManager;
    }

    public IActionResult Login()
    {
        TempData["ReturnUrl"] =
            Request.Query.Keys.Contains("ReturnUrl") ? Request.Query["ReturnUrl"].FirstOrDefault() : "";
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Login(LoginViewModel model)
    {
        if (ModelState.IsValid)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            var res = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe,
                false);
            if (res.Succeeded)
            {
                return RedirectToAction("Index", "Home", new { area = "User" });
            }
            else
            {
                ModelState.AddModelError("", "Failed to login.");
            }
        }

        return View();
    }

    public async Task<IActionResult> Logout()
    {
        if (User.Identity!.IsAuthenticated)
            await _signInManager.SignOutAsync();

        return RedirectToAction("Index", "Home", new { area = "User" });
    }
}