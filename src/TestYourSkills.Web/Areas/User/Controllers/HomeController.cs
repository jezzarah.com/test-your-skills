﻿using System.Text.Json;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TestYourSkills.Core.Enums;
using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Models.Questions;
using TestYourSkills.Core.ViewModels;

namespace TestYourSkills.Web.Areas.User.Controllers;

[Area("User")]
public class HomeController : Controller
{
    //private static readonly Dictionary<string, IEnumerable<Question>> Cache = new();
    
    private readonly IQuestionService _questionService;
    private readonly IMapper _mapper;
    private readonly ICacheService _cache;

    public HomeController(IQuestionService questionService, IMapper mapper, ICacheService cache)
    {
        _questionService = questionService;
        _mapper = mapper;
        _cache = cache;
    }

    public async Task<IActionResult> Index()
    {
        HomeViewModel homeVm = new HomeViewModel();
        List<TopicViewModel> topicsVm = new ();
        var topics = await _questionService.GetTopicsAsync();
        if (topics != null)
        {
            foreach (var topic in topics)
            {
                TopicViewModel topicVm = new ()
                {
                    Id = topic!.Id,
                    Name = topic.Name
                };
                if (topic.Questions != null)
                {
                    topicVm.QuestionsCount = topic.Questions.Count;
                    topicsVm.Add(topicVm);
                }
            }
        }

        homeVm.TopicsVM.AddRange(topicsVm);
        homeVm.QuestionsCount = topicsVm.Sum(t => t.QuestionsCount);

        return View(homeVm);
    }

    public async Task<IActionResult> Section(int id)
    {
        SectionViewModel sectionVm = new();
        var topics = await _questionService.GetTopicsAsync();
        if (topics != null)
        {
            if (id > 0)
            {
                var topic = topics.FirstOrDefault(t => t!.Id == id);
                if (topic != null)
                {
                    sectionVm.TopicVM.Id = topic.Id;
                    sectionVm.TopicVM.Name = topic.Name;
                    sectionVm.MaxQuestionsCount = topic.Questions!.Count();
                }
            }
            else
            {
                sectionVm.TopicVM.Name = "All Topics";
                sectionVm.MaxQuestionsCount = topics.Sum(topic => topic!.Questions?.Count ?? 0);
            }
        }

        return View(sectionVm);
    }

    [HttpPost]
    public IActionResult Section(SectionViewModel sectionVM)
    {
        TempData["sectionVM"] = JsonSerializer.Serialize(sectionVM);
        return RedirectToAction(nameof(Quiz));
    }

    [HttpGet]
    public async Task<IActionResult> Quiz()
    {
        QuizViewModel quizViewModel = new();
        if (TempData["sectionVM"] != null)
        {
            var objectVm = JsonSerializer.Deserialize<SectionViewModel>(TempData["sectionVM"]!.ToString()!);
            if (objectVm != null)
            {
                var questions =  objectVm.TopicVM.Id == 0 
                    ? await _questionService.GetRandomQuestionsAsync(objectVm.UserQuestionsCount) 
                    : await _questionService.GetRandomQuestionsForTopicAsync(objectVm.TopicVM.Id,
                        objectVm.UserQuestionsCount);

                var quizId = Guid.NewGuid().ToString();
                await PrepareQuizModelForUser(quizId, objectVm.TopicVM.Name, questions!);
                quizViewModel.QuizId = quizId;
                quizViewModel.TotalQuestionsCount = objectVm.UserQuestionsCount;
                quizViewModel.CurrentQuestion = 1;
                quizViewModel.TopicName = objectVm.TopicVM.Name;
                quizViewModel.Question = _mapper.Map<QuizQuestionViewModel>(questions[0]);
            }
        }

        return View(quizViewModel);
    }

    [HttpPost]
    [ActionName("Quiz")]
    public async Task<IActionResult> Quiz(QuizViewModel model, QuizActionType actionId)
    {
        async Task UpdateModelQuestion()
        {
            var quizProcess = await _cache.GetCacheAsync<QuizProcessViewModel>(model.QuizId!);
            var currentQuestion = quizProcess!.Questions!.ToArray()[model.CurrentQuestion - 1];
            
            await UpdateQuizProcessInfo(quizProcess, model, actionId, currentQuestion);
            //model.Question = _mapper.Map<QuizQuestionViewModel>(questions!.ToArray()[model.CurrentQuestion]);
            // next Question
            if (model.TotalQuestionsCount == model.CurrentQuestion)
                return;
            
            model.Question = _mapper.Map<QuizQuestionViewModel>(quizProcess.Questions!.ToArray()[model.CurrentQuestion]);
        }

        switch (actionId)
        {
            case QuizActionType.CheckAnswer:
                var questions = await _cache.GetCacheAsync<IEnumerable<Question>>(model.QuizId!);
                var currentQuestion = questions!.ToArray()[model.CurrentQuestion - 1];
                var rightAnswers = currentQuestion.QuestionAnswers!.Where(qa =>
                    qa.CorrectAnswer).ToList();
                model.MessageForUser = string.Join("<br />", rightAnswers.Select(ra => $"Answer [{ra.Message}]"));
                model.Question = _mapper.Map<QuizQuestionViewModel>(currentQuestion);
                ModelState.Clear();
                break;
            case QuizActionType.DontKnow:
                //model.MessageForUser = "Don't be upset, repeat and you'll be better";
                await UpdateModelQuestion();
                
                if (model.TotalQuestionsCount == model.CurrentQuestion)
                    return RedirectToAction(nameof(QuizCompleted), new { quizId = model.QuizId });
                
                model.CurrentQuestion++;
                ModelState.Clear();
                break;
            case QuizActionType.GoNextQuestion:
                if (model.QuizId == null/* || !Cache.ContainsKey(model.QuizId) */)
                    return RedirectToAction(nameof(Index));
                
                await UpdateModelQuestion();
                if (model.TotalQuestionsCount == model.CurrentQuestion)
                    return RedirectToAction(nameof(QuizCompleted), new { quizId = model.QuizId });
                
                model.CurrentQuestion++;
                ModelState.Clear();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(actionId), actionId, null);
        }

        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> QuizCompleted(string quizId)
    {
        var quiz = await _cache.GetCacheAsync<QuizProcessViewModel>(quizId);
        if (quiz == null)
           return NotFound();

        return View(quiz);
    }

    private async Task UpdateQuizProcessInfo(QuizProcessViewModel quizProcess, QuizViewModel quiz, QuizActionType quizActionType,
        Question currentQuestion)
    {
        // Validate Answer
        QuizProcessUserAnswerViewModel quizUserAnswer = new() { QuestionId = quiz.Question.Id, Ids = quiz.UserAnswer?.Ids, ActionType = quizActionType};
        var rightAnswers = currentQuestion.QuestionAnswers!.Where(qa =>
            qa.CorrectAnswer).ToList();
        
        if(quizUserAnswer.Ids != null && quizUserAnswer.Ids.Length == rightAnswers.Count 
                                      && rightAnswers.All(i=> quizUserAnswer.Ids.Contains(i.Id)))
        {
            quizProcess.CorrectAnswersCount++;
            quizUserAnswer.AnswerIsCorrect = true;
        }
        
        quizProcess.UserAnswers.Add(quizUserAnswer);
        
        // Add Answer to Cache
        await _cache.SetCacheAsync(quiz.QuizId!, quizProcess, TimeSpan.FromHours(1));
    }
    
    private async Task PrepareQuizModelForUser(string quizId, string topicName, IEnumerable<Question> questions)
    {
        QuizProcessViewModel quizVm = new()
        {
            QuizId = quizId,
            TopicName = topicName,
            Questions = questions,

        };
        
        await _cache.SetCacheAsync(quizId, quizVm, TimeSpan.FromHours(1));
    }
}