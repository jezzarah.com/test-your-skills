﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Models.Questions;
using TestYourSkills.Core.ViewModels;

namespace TestYourSkills.Web.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = "Admin,Redactor")]
public class TopicController : Controller
{
    private readonly IMapper _mapper;
    private readonly IQuestionService _questionService;

    public TopicController(IMapper mapper, IQuestionService questionService)
    {
        _mapper = mapper;
        _questionService = questionService;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var topics = await _questionService.GetTopicsAsync();
        var topicVm = _mapper.Map<IEnumerable<TopicViewModel>>(topics);

        return View(topicVm);
    }

    [HttpGet]
    public IActionResult AddTopic()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> AddTopic(AddTopicViewModel model)
    {
        if (ModelState.IsValid)
        {
            try
            {
                var topic = _mapper.Map<Topic>(model);
                var newTopic = await _questionService.AddTopicAsync(topic);
                if (topic != null)
                    return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.InnerException != null ? exc.InnerException?.Message! : exc.Message);
            }
        }

        return View();
    }

    public async Task<IActionResult> UpdateTopic(int id, string name)
    {
        UpdateTopicViewModel topicVm = new() { Id = id, Name = name };

        try
        {
            var topic = await _questionService.GetQuestionsForTopicAsync(id);
            topicVm.Questions = _mapper.Map<IEnumerable<UpdateTopicQuestionViewModel>>(topic);
        }
        catch (Exception exc)
        {
            ModelState.AddModelError("", exc.InnerException != null ? exc.InnerException?.Message! : exc.Message);
        }

        return View(topicVm);
    }

    [HttpPost]
    public async Task<IActionResult> UpdateTopic(AddTopicViewModel model)
    {
        if (ModelState.IsValid)
        {
            try
            {
                var topic = _mapper.Map<Topic>(model);
                var updatedTopic = await _questionService.UpdateTopicAsync(topic);
                if (topic != null)
                    return View();
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.InnerException != null ? exc.InnerException?.Message! : exc.Message);
            }
        }

        return View();
    }

    [HttpPost]
    public async Task<IActionResult> UpdateTopicTitle(UpdateTopicTitleViewModel model)
    {
        if (ModelState.IsValid)
        {
            try
            {
                var topic = _mapper.Map<Topic>(model);
                var updatedTopic = await _questionService.UpdateTopicAsync(topic);
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.InnerException != null ? exc.InnerException?.Message! : exc.Message);
            }
        }

        return RedirectToAction(nameof(UpdateTopic),
            new { id = model.Id, name = model.Name });
    }

    public async Task<IActionResult> DeleteTopic(int id)
    {
        try
        {
            var deleted = await _questionService.RemoveTopicAsync(id);
            if (!deleted)
                ModelState.AddModelError("", "Topic not deleted");
        }
        catch (Exception exc)
        {
            ModelState.AddModelError("", exc.InnerException != null ? exc.InnerException?.Message! : exc.Message);
        }

        return RedirectToAction(nameof(Index));
    }

    [HttpGet]
    public async Task<IActionResult> AddQuestion(int topicId, string topicName)
    {
        AddTopicQuestionViewModel questionVm = new() { TopicId = topicId, TopicName = topicName };
        return View(questionVm);
    }

    [HttpPost]
    public async Task<IActionResult> AddQuestion(AddTopicQuestionViewModel model)
    {
        AddTopicQuestionViewModel topicVm = new();

        try
        {
            var topicQuestion = _mapper.Map<Question>(model);
            var added = await _questionService.AddQuestionsAsync(new List<Question>() { topicQuestion });

            return RedirectToAction(nameof(UpdateTopic), new { id = model.TopicId, name = model.TopicName });
        }
        catch (Exception exc)
        {
            if (exc.InnerException != null)
                ModelState.AddModelError("", exc.InnerException?.Message!);
            else
                ModelState.AddModelError("", exc.Message);
        }

        return View(topicVm);
    }

    [HttpGet]
    public async Task<IActionResult> UpdateQuestion(int topicId, string topicName, int questionId)
    {
        var questions = await _questionService.GetQuestionsForTopicAsync(topicId);
        var question = questions.Where(q => q.Id == questionId).First();
        var questionVm = _mapper.Map<UpdateTopicQuestionViewModel>(question);
        questionVm.TopicName = topicName;
        return View(questionVm);
    }

    [HttpPost]
    public async Task<IActionResult> UpdateQuestion(UpdateTopicQuestionViewModel model)
    {
        UpdateTopicQuestionViewModel topicVm = new();
        try
        {
            var topicQuestion = _mapper.Map<Question>(model);
            var question = await _questionService.GetQuestionAsync(model.Id);
            List<int> idsForRemove = new();
            foreach (var qa in question.QuestionAnswers)
            {
                var questionForUpdate = model.QuestionAnswers!.FirstOrDefault(q => q.Id == qa.Id);
                if (questionForUpdate == null)
                    idsForRemove.Add(qa.Id);
                else
                {
                    qa.CorrectAnswer = questionForUpdate.CorrectAnswer;
                    qa.Message = questionForUpdate.Message;
                }
            }

            foreach (var id in idsForRemove)
                await _questionService.RemoveQuestionAnswerAsync(id);

            await _questionService.AddQuestionAnswersAsync(topicQuestion.QuestionAnswers!
                .Where(q => q.Id == 0)
                .Select(qa => new QuestionAnswer
                    { QuestionId = question.Id, CorrectAnswer = qa.CorrectAnswer, Message = qa.Message }));

            question.Title = topicQuestion.Title;
            question.Details = topicQuestion.Details;
            question.MultiAnswers = topicQuestion.MultiAnswers;

            var updated = await _questionService.UpdateQuestionAsync(question);
            return RedirectToAction(nameof(UpdateTopic), new { id = model.TopicId, name = model.TopicName });
        }
        catch (Exception exc)
        {
            if (exc.InnerException != null)
                ModelState.AddModelError("", exc.InnerException?.Message!);
            else
                ModelState.AddModelError("", exc.Message);
        }

        return View(topicVm);
    }


    public IActionResult AddQuestionPartial(string answerId, bool isUpdate)
    {
        BaseTopicQuestionAnswerViewModel answerViewModel = isUpdate
            ? new UpdateTopicQuestionAnswerViewModel()
            : new AddTopicQuestionAnswerViewModel();
        ViewBag.AnswerId = answerId; // Pass the answerId to the partial view
        return PartialView("_QuestionAnswerPartial", answerViewModel);
    }

    public async Task<IActionResult> DeleteQuestion(int topicId, string topicName, int questionId)
    {
        try
        {
            var question = await _questionService.GetQuestionAsync(questionId);
            foreach (var questionAnswerId in question.QuestionAnswers.Select(qa => qa.Id).ToList())
                await _questionService.RemoveQuestionAnswerAsync(questionAnswerId);

            var deleted = await _questionService.RemoveQuestionAsync(questionId);
            if (!deleted)
                ModelState.AddModelError("", "Question not deleted");
        }
        catch (Exception exc)
        {
            ModelState.AddModelError("", exc.InnerException?.Message!);
        }

        return RedirectToAction(nameof(UpdateTopic), new { id = topicId, name = topicName });
    }
}