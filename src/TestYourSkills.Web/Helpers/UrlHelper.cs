﻿namespace TestYourSkills.Web.Helpers;

public static class UrlHelper
{
    public static string GetCurrentUrl(IHttpContextAccessor httpContextAccessor)
    {
        var httpContext = httpContextAccessor.HttpContext;
        var currentUrl = $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";
        return currentUrl;
    }
}