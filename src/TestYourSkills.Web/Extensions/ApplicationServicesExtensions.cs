﻿using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;
using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Services;
using TestYourSkills.Data.Data;
using TestYourSkills.Data.Services;
using TestYourSkills.Web.Services;

namespace TestYourSkills.Web.Extensions;

public static class ApplicationServicesExtensions
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAutoMapper(typeof(MappingProfiles));
        
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IQuestionService, QuestionService>();
        services.AddScoped<IRandomizer, Randomizer>();
        
        services.AddDbContext<DataContext>(o =>
        {
            o.UseSqlite(configuration.GetConnectionString("DataConnection") ??
                        throw new ArgumentException("Connection string 'DataConnection' is empty"));
        });
        
        services.AddSingleton<ICacheService, CacheService>();
        
        services.AddSingleton<IConnectionMultiplexer>(s =>
        {
            var configurationOptions = ConfigurationOptions.Parse(configuration.GetConnectionString("Redis"), true);
            return ConnectionMultiplexer.Connect(configurationOptions);
        });
        
        return services;
    }
}