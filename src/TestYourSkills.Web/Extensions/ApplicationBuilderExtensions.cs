﻿namespace TestYourSkills.Web.Extensions;

public static class ApplicationBuilderExtensions
{
    public static WebApplicationBuilder ApplicationSetup(this WebApplicationBuilder builder)
    {
        string dataDirectory = builder.Configuration.GetValue<string>("DataDirectory");
        Directory.CreateDirectory(dataDirectory);

        return builder;
    }
}