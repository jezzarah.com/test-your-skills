﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TestYourSkills.Core.Models.Identity;
using TestYourSkills.Data.Identity;

namespace TestYourSkills.Web.Extensions;

public static class IdentityServiceExtensions
{
    public static IServiceCollection AddIdentityServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<IdentityContext>(o =>
        {
            o.UseSqlite(configuration.GetConnectionString("IdentityConnection") ??
                        throw new ArgumentException("Connection string 'IdentityConnection' is empty"));
        });

        services.AddAuthentication();
        services.AddScoped<RoleManager<IdentityRole>>();
        
        services.PostConfigure<CookieAuthenticationOptions>(IdentityConstants.ApplicationScheme,
            options =>
            {
                options.LoginPath = "/Identity/Account/Login";
                options.LogoutPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/AccessDenied";
            });
        
        services.AddIdentity<AppUser, IdentityRole>(o =>
            {
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
            })
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<IdentityContext>();
        //builder = new IdentityBuilder(builder.UserType, services);
        //builder.AddEntityFrameworkStores<IdentityContext>();
        //builder.AddSignInManager<SignInManager<AppUser>>();

        return services;
    }
}