﻿using AutoMapper;
using TestYourSkills.Core.Models.Questions;
using TestYourSkills.Core.ViewModels;

namespace TestYourSkills.Web.Services;

public class MappingProfiles : Profile
{
    public MappingProfiles()
    {
        #region Topic

        CreateMap<AddTopicViewModel, Topic>().ForMember(d=>d.Name, 
                o=> o.MapFrom(s=>s.TopicName))
            .ReverseMap();
        CreateMap<Topic, TopicViewModel>().ReverseMap();
        CreateMap<Topic, UpdateTopicViewModel>().ReverseMap();
        CreateMap<Topic, UpdateTopicTitleViewModel>().ReverseMap();

        #endregion
        
        #region Question
        
        CreateMap<Question, QuizQuestionViewModel>().ReverseMap();
        CreateMap<Question, BaseTopicQuestionViewModel>().ReverseMap();
        CreateMap<Question, UpdateTopicQuestionViewModel>().ReverseMap();
        CreateMap<Question, AddTopicQuestionViewModel>().ReverseMap();

        #endregion

        #region QuestionAnswers

        CreateMap<QuestionAnswer, BaseTopicQuestionAnswerViewModel>().ReverseMap();
        CreateMap<QuestionAnswer, UpdateTopicQuestionAnswerViewModel>().ReverseMap();
        CreateMap<QuestionAnswer, AddTopicQuestionAnswerViewModel>().ReverseMap();
        CreateMap<QuestionAnswer, QuizQuestionAnswerViewModel>().ReverseMap();

        #endregion
    }
}