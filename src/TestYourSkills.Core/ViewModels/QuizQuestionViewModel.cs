﻿namespace TestYourSkills.Core.ViewModels;

public class QuizQuestionViewModel
{
    public int Id { get; set; }
    public string Title { get; set; } = null!;
    public string? Details { get; set; }
    public bool MultiAnswers { get; set; }
    public IEnumerable<QuizQuestionAnswerViewModel>? QuestionAnswers { get; set; }
}