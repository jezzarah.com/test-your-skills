﻿using System.ComponentModel.DataAnnotations;

namespace TestYourSkills.Core.ViewModels;

public class SectionViewModel
{
    public TopicViewModel TopicVM { get; set; } = new TopicViewModel();
    public int MaxQuestionsCount { get; set; }
    public int UserQuestionsCount { get; set; }
}
