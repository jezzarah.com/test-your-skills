using System.ComponentModel.DataAnnotations;

namespace TestYourSkills.Core.ViewModels;

public class LoginViewModel
{
    [Required] public string Username { get; set; } = null!;
    [Required] public string Password { get; set; } = null!;
    [Required] public bool RememberMe { get; set; }
}