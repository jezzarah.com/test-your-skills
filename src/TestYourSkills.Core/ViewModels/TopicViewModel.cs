﻿namespace TestYourSkills.Core.ViewModels;

public class TopicViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public int QuestionsCount { get; set; }
}
