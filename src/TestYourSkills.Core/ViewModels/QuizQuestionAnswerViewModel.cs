﻿namespace TestYourSkills.Core.ViewModels;

public class QuizQuestionAnswerViewModel
{
    public int Id { get; set; }
    public string Message { get; set; } = null!;
}