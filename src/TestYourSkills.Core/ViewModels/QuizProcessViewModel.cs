﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.ViewModels;

public class QuizProcessViewModel
{
    public string QuizId { get; set; } = null!;
    public string TopicName { get; set; } = null!;
    public IEnumerable<Question>? Questions { get; set; }
    public List<QuizProcessUserAnswerViewModel> UserAnswers { get; set; } = new();
    public int CorrectAnswersCount { get; set; }
    public int PercentageCorrectAnswers { get; set; }
    public string? MessageForUser { get; set; }
}