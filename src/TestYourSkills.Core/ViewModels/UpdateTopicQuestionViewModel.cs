﻿namespace TestYourSkills.Core.ViewModels;

public class UpdateTopicQuestionViewModel : BaseTopicQuestionViewModel
{
    public int Id { get; set; }
    public IEnumerable<UpdateTopicQuestionAnswerViewModel>? QuestionAnswers { get; set; }
}