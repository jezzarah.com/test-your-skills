﻿using TestYourSkills.Core.Enums;

namespace TestYourSkills.Core.ViewModels;

public class QuizProcessUserAnswerViewModel
{
    public int QuestionId { get; set; }
    public int[] Ids { get; set; }
    public bool AnswerIsCorrect { get; set; }
    public QuizActionType ActionType { get; set; }
}