﻿namespace TestYourSkills.Core.ViewModels
{
    public class BaseTopicQuestionAnswerViewModel
    {
        public string Message { get; set; } = null!;
        public bool CorrectAnswer { get; set; }
    }
}
