﻿using System.ComponentModel.DataAnnotations;

namespace TestYourSkills.Core.ViewModels;

public class UpdateTopicTitleViewModel
{
    public int Id { get; set; }
    [Required]
    [MaxLength(250)]
    public string Name { get; set; } = null!;
}