﻿namespace TestYourSkills.Core.ViewModels;

public class UpdateTopicViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public IEnumerable<UpdateTopicQuestionViewModel>? Questions { get; set; }
}