﻿namespace TestYourSkills.Core.ViewModels;

public class HomeViewModel
{
    public List<TopicViewModel> TopicsVM { get; set; } = new List<TopicViewModel>();
    public int QuestionsCount { get; set; }
}
