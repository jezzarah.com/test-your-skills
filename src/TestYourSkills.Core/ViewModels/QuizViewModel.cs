﻿namespace TestYourSkills.Core.ViewModels;

public class QuizViewModel
{
    public string TopicName { get; set; } = null!;
    public QuizQuestionViewModel Question { get; set; } = new();
    public QuizUserAnswerViewModel UserAnswer { get; set; } = new();
    public int TotalQuestionsCount { get; set; }
    public int CurrentQuestion { get; set; }
    public string? QuizId { get; set; }
    public string? MessageForUser { get; set; }
}