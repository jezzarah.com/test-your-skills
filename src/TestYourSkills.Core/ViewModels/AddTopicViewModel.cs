﻿using System.ComponentModel.DataAnnotations;

namespace TestYourSkills.Core.ViewModels;

public class AddTopicViewModel
{
    [Required]
    [MaxLength(250)]
    public string TopicName { get; set; } = null!;
}