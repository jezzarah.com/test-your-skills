﻿namespace TestYourSkills.Core.ViewModels
{
    public class BaseTopicQuestionViewModel
    {
        public int TopicId { get; init; }
        public string TopicName { get; set; } = null!;
        public string Title { get; set; } = null!;
        public string? Details { get; set; }
        public bool MultiAnswers { get; set; }
        public IEnumerable<BaseTopicQuestionAnswerViewModel>? QuestionAnswers { get; set; }
    }
}
