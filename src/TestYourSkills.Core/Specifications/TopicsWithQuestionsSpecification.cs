﻿using System.Linq.Expressions;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications;

public class TopicsWithQuestionsSpecification : BaseSpecification<Topic>
{
    public TopicsWithQuestionsSpecification()
    {
        AddInclude(t=>t.Questions);
    }
}