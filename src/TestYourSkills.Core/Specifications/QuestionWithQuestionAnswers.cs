﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications
{
    internal class QuestionWithQuestionAnswers : BaseSpecification<Question>
    {
        public QuestionWithQuestionAnswers(int questionId) : base(q=>q.Id == questionId)
        {
            AddInclude(q => q.QuestionAnswers);
        }
    }
}
