﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications;

public class SpecificQuestionsByTopicIdWithQuestionAnswersSpecification : BaseSpecification<Question>
{
    public SpecificQuestionsByTopicIdWithQuestionAnswersSpecification(int topicId, IEnumerable<int> ids) : base(q=> q.TopicId == topicId && ids.Contains(q.Id))
    {
        AddInclude(q=>q.QuestionAnswers);
    }
}