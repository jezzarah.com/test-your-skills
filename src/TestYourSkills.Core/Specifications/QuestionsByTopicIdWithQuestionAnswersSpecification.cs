﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications;

public class QuestionsByTopicIdWithQuestionAnswersSpecification : BaseSpecification<Question>
{
    public QuestionsByTopicIdWithQuestionAnswersSpecification(int topicId) : base(q => q.TopicId == topicId)
    {
        AddInclude(q=>q.QuestionAnswers);
    }
}