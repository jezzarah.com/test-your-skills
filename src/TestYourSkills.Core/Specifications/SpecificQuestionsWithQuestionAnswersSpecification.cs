﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications;

public class SpecificQuestionsWithQuestionAnswersSpecification : BaseSpecification<Question>
{
    public SpecificQuestionsWithQuestionAnswersSpecification(IEnumerable<int> ids) : base(q => ids.Contains(q.Id))
    {
        AddInclude(q=>q.QuestionAnswers);
    }
}