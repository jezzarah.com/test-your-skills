﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Specifications;

public class QuestionsByTopicIdSpecification : BaseSpecification<Question>
{
    public QuestionsByTopicIdSpecification(int topicId) : base(q => q.TopicId == topicId)
    {
    }
}