﻿using Microsoft.AspNetCore.Identity;

namespace TestYourSkills.Core.Models.Identity;

public class AppUser : IdentityUser
{
    public string DisplayName { get; set; } = null!;
}