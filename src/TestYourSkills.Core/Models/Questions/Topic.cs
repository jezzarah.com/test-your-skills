﻿namespace TestYourSkills.Core.Models.Questions;

public class Topic : BaseEntity
{
    public string Name { get; set; } = null!;
    public IReadOnlyList<Question>? Questions { get; set; }
}