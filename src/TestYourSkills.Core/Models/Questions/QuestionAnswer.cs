﻿namespace TestYourSkills.Core.Models.Questions;

public class QuestionAnswer : BaseEntity
{
    public string Message { get; set; } = null!;
    public bool CorrectAnswer { get; set; }
    public int QuestionId { get; set; }
}