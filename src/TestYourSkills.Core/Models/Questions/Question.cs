﻿namespace TestYourSkills.Core.Models.Questions;

public class Question : BaseEntity
{
    public string Title { get; set; } = null!;
    public string? Details { get; set; }
    public bool MultiAnswers { get; set; }
    public int TopicId { get; set; }
    public IReadOnlyList<QuestionAnswer>? QuestionAnswers { get; set; }
}