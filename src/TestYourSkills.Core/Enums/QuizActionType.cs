﻿namespace TestYourSkills.Core.Enums;

public enum QuizActionType
{
    CheckAnswer,
    DontKnow,
    GoNextQuestion
}