﻿using TestYourSkills.Core.Interfaces;

namespace TestYourSkills.Core.Services;

public class Randomizer : IRandomizer
{
    public async Task<IEnumerable<int>> GenerateRandomIntAsync(int min, int max, int count)
    {
        var random = new Random();
        var randomArray = new int[count];

        for (var i = 0; i < count; i++)
            randomArray[i] = await Task.Run(() => random.Next(min, max + 1));

        return randomArray;
    }

    public async Task<IEnumerable<T>> GetRandomElementsAsync<T>(IEnumerable<T> collection, int count)
    {
        await Task.CompletedTask;
        var random = new Random();
        var randomElements = collection.ToList();

        if (count >= randomElements.Count)
        {
            for (int i = 0; i < randomElements.Count; i++)
            {
                int randomIndex = random.Next(i, randomElements.Count);

                (randomElements[i], randomElements[randomIndex]) = (randomElements[randomIndex], randomElements[i]);
            }

            return randomElements;
        }
        
        for (int i = 0; i < randomElements.Count - count; i++)
        {
            int randomIndex = random.Next(i, randomElements.Count);
            
            // T temp = randomElements[i];
            // randomElements[i] = randomElements[randomIndex];
            // randomElements[randomIndex] = temp;
            // the same is:
            (randomElements[i], randomElements[randomIndex]) = (randomElements[randomIndex], randomElements[i]);
        }

        return randomElements.Take(count);
    }
}