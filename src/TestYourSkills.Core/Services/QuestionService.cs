﻿using TestYourSkills.Core.Interfaces;
using TestYourSkills.Core.Models.Questions;
using TestYourSkills.Core.Specifications;

namespace TestYourSkills.Core.Services;

public class QuestionService : IQuestionService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IRandomizer _randomizer;

    public QuestionService(IUnitOfWork unitOfWork, IRandomizer randomizer)
    {
        _unitOfWork = unitOfWork;
        _randomizer = randomizer;
    }

    #region Topics

    public async Task<IReadOnlyList<Topic?>> GetTopicsAsync()
    {
        var spec = new TopicsWithQuestionsSpecification();
        var topics = await _unitOfWork.Repository<Topic>().ListAllWithSpec(spec);
        return topics;
    }

    public async Task<IReadOnlyList<Question?>> GetQuestionsForTopicAsync(int topicId)
    {
        var spec = new QuestionsByTopicIdWithQuestionAnswersSpecification(topicId);
        var questions = await _unitOfWork.Repository<Question>().ListAllWithSpec(spec);
        return questions;
    }

    public async Task<IReadOnlyList<Question?>> GetRandomQuestionsAsync(int questionCount)
    {
        var questions = await _unitOfWork.Repository<Question>().ListAllAsync();
        var randomQuestions = await _randomizer.GetRandomElementsAsync(questions, questionCount);
        var spec = new SpecificQuestionsWithQuestionAnswersSpecification(randomQuestions.Select(q => q.Id));
        var randomQuestionsWithQuestionAnswers = await _unitOfWork.Repository<Question>().ListAllWithSpec(spec);
        foreach (var randomQuestionsWithQuestionAnswer in randomQuestionsWithQuestionAnswers)
        {
            if (randomQuestionsWithQuestionAnswer?.QuestionAnswers != null &&
                randomQuestionsWithQuestionAnswer.QuestionAnswers.Any())
            {
                var randomList = await _randomizer.GetRandomElementsAsync(
                    randomQuestionsWithQuestionAnswer.QuestionAnswers, randomQuestionsWithQuestionAnswer.QuestionAnswers.Count);
                randomQuestionsWithQuestionAnswer.QuestionAnswers = randomList.ToList();
            }
        }

        return randomQuestionsWithQuestionAnswers;
    }

    public async Task<IReadOnlyList<Question?>> GetRandomQuestionsForTopicAsync(int topicId, int questionCount)
    {
        var spec = new QuestionsByTopicIdWithQuestionAnswersSpecification(topicId);
        var questions = await _unitOfWork.Repository<Question>().ListAllWithSpec(spec);

        // Add Cache
        // var specForSpecificIds = new SpecificQuestionsByTopicIdWithQuestionAnswersSpecification(topicId, questionCount);
        // var questions = await _unitOfWork.Repository<Question>().ListAllWithSpec(specForSpecificIds);
        var randomQuestions = await _randomizer.GetRandomElementsAsync(questions, questionCount);
        foreach (var randomQuestionsWithQuestionAnswer in randomQuestions)
        {
            if (randomQuestionsWithQuestionAnswer?.QuestionAnswers != null &&
                randomQuestionsWithQuestionAnswer.QuestionAnswers.Any())
            {
                var randomList = await _randomizer.GetRandomElementsAsync(
                    randomQuestionsWithQuestionAnswer.QuestionAnswers, randomQuestionsWithQuestionAnswer.QuestionAnswers.Count);
                randomQuestionsWithQuestionAnswer.QuestionAnswers = randomList.ToList();
            }
        }

        return randomQuestions.ToList();
    }

    public async Task<Topic> AddTopicAsync(Topic topic)
    {
        _unitOfWork.Repository<Topic>().Add(topic);
        await _unitOfWork.Complete();
        return topic;
    }

    public async Task<Topic> UpdateTopicAsync(Topic topic)
    {
        _unitOfWork.Repository<Topic>().Update(topic);
        await _unitOfWork.Complete();
        return topic;
    }

    public async Task<bool> RemoveTopicAsync(int topicId)
    {
        var topic = await _unitOfWork.Repository<Topic>().GetByIdAsync(topicId);
        if (topic == null)
            return false;

        _unitOfWork.Repository<Topic>().Delete(topic);
        await _unitOfWork.Complete();
        return true;
    }

    #endregion

    #region Questions

    public async Task<Question?> GetQuestionAsync(int questionId)
    {
        var spec = new QuestionWithQuestionAnswers(questionId);
        var question = await _unitOfWork.Repository<Question>().GetEntityWithSpec(spec);
        return question;
    }

    public async Task<IEnumerable<Question>> AddQuestionsAsync(IEnumerable<Question> questions)
    {
        foreach (var question in questions)
            _unitOfWork.Repository<Question>().Add(question);

        await _unitOfWork.Complete();
        return questions;
    }

    public async Task<Question> UpdateQuestionAsync(Question question)
    {
        _unitOfWork.Repository<Question>().Update(question);
        await _unitOfWork.Complete();
        return question;
    }

    public async Task<bool> RemoveQuestionAsync(int questionId)
    {
        var question = await _unitOfWork.Repository<Question>().GetByIdAsync(questionId);
        if (question == null)
            return false;

        _unitOfWork.Repository<Question>().Delete(question);
        await _unitOfWork.Complete();
        return true;
    }

    #endregion

    #region QuestionAnswers

    public async Task<IEnumerable<QuestionAnswer>> AddQuestionAnswersAsync(IEnumerable<QuestionAnswer> questionAnswers)
    {
        foreach (var questionAnswer in questionAnswers)
            _unitOfWork.Repository<QuestionAnswer>().Add(questionAnswer);

        await _unitOfWork.Complete();
        return questionAnswers;
    }

    public async Task<QuestionAnswer> UpdateQuestionAnswerAsync(QuestionAnswer questionAnswer)
    {
        _unitOfWork.Repository<QuestionAnswer>().Update(questionAnswer);
        await _unitOfWork.Complete();
        return questionAnswer;
    }

    public async Task<bool> RemoveQuestionAnswerAsync(int answerId)
    {
        var questionAnswer = await _unitOfWork.Repository<QuestionAnswer>().GetByIdAsync(answerId);
        if (questionAnswer == null)
            return false;

        _unitOfWork.Repository<QuestionAnswer>().Delete(questionAnswer);
        await _unitOfWork.Complete();
        return true;
    }

    #endregion
}