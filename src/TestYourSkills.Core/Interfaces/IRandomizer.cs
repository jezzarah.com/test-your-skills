﻿namespace TestYourSkills.Core.Interfaces;

public interface IRandomizer
{
    Task<IEnumerable<int>> GenerateRandomIntAsync(int min, int max, int count);
    Task<IEnumerable<T>> GetRandomElementsAsync<T>(IEnumerable<T> collection, int count);
}