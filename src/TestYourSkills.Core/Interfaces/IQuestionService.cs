﻿using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Core.Interfaces;

public interface IQuestionService
{
    #region Topics

    Task<IReadOnlyList<Topic?>> GetTopicsAsync();
    Task<IReadOnlyList<Question?>> GetQuestionsForTopicAsync(int topicId);
    Task<IReadOnlyList<Question?>> GetRandomQuestionsAsync(int questionCount);
    Task<IReadOnlyList<Question?>> GetRandomQuestionsForTopicAsync(int topicId, int questionCount);
    Task<Topic> AddTopicAsync(Topic topic);
    Task<Topic> UpdateTopicAsync(Topic topic);
    Task<bool> RemoveTopicAsync(int topicId);

    #endregion

    #region Questions

    Task<Question?> GetQuestionAsync(int questionId);
    Task<IEnumerable<Question>> AddQuestionsAsync(IEnumerable<Question> questions);
    Task<Question> UpdateQuestionAsync(Question question);
    Task<bool> RemoveQuestionAsync(int questionId);
    
    #endregion

    #region QuestionAnswers
    
    Task<IEnumerable<QuestionAnswer>> AddQuestionAnswersAsync(IEnumerable<QuestionAnswer> questionAnswers);
    Task<QuestionAnswer> UpdateQuestionAnswerAsync(QuestionAnswer questionAnswer);
    Task<bool> RemoveQuestionAnswerAsync(int answerId);

    #endregion
}