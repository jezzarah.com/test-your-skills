﻿namespace TestYourSkills.Core.Interfaces;

public interface ICacheService
{
    Task<T?> GetCacheAsync<T>(string cacheKey);
    Task<bool> SetCacheAsync<T>(string cacheKey, T data, TimeSpan timeToLive);
    Task<bool> DeleteCacheAsync(string cacheKey);
}