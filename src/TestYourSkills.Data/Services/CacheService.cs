﻿using System.Text.Json;
using StackExchange.Redis;
using TestYourSkills.Core.Interfaces;

namespace TestYourSkills.Data.Services;

public class CacheService : ICacheService
{
    private readonly IDatabase _database;

    public CacheService(IConnectionMultiplexer redis)
    {
        _database = redis.GetDatabase();
    }

    public async Task<T?> GetCacheAsync<T>(string cacheKey)
    {
        var data = await _database.StringGetAsync(cacheKey);
        return data.IsNullOrEmpty ? default(T) : JsonSerializer.Deserialize<T>(data);
    }
    
    public async Task<bool> SetCacheAsync<T>(string cacheKey, T data, TimeSpan timeToLive)
    {
        var created = await _database.StringSetAsync(cacheKey,
            JsonSerializer.Serialize(data),
            timeToLive);

        return created;
    }

    public async Task<bool> DeleteCacheAsync(string cacheKey)
    {
        var deleted = await _database.KeyDeleteAsync(cacheKey);
        return deleted;
    }
}