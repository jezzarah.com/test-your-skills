﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using TestYourSkills.Core.Models.Identity;

namespace TestYourSkills.Data.Identity;

public class IdentityContextSeed
{
    public static async Task SeedUsersAsync(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager,
        IConfiguration configuration)
    {
        if (!roleManager.Roles.Any())
        {
            await roleManager.CreateAsync(new IdentityRole() { Name = "Admin" });
            await roleManager.CreateAsync(new IdentityRole() { Name = "Redactor" });
        }

        if (!userManager.Users.Any())
        {
            await CreateUser(userManager,
                configuration.GetSection("UsersConfig:Admin:UserName").Value,
                configuration.GetSection("UsersConfig:Admin:DisplayName").Value,
                configuration.GetSection("UsersConfig:Admin:Password").Value,
                configuration.GetSection("UsersConfig:Admin:Role").Value);

            await CreateUser(userManager,
                configuration.GetSection("UsersConfig:Redactor:UserName").Value,
                configuration.GetSection("UsersConfig:Redactor:DisplayName").Value,
                configuration.GetSection("UsersConfig:Redactor:Password").Value,
                configuration.GetSection("UsersConfig:Redactor:Role").Value);
        }
    }

    private static async Task CreateUser(UserManager<AppUser> userManager, string username,
       string displayName, string password, string role)
    {
        AppUser user = new()
        {
            UserName = username,
            DisplayName = displayName,
        };

        await userManager.CreateAsync(user, password);
        await userManager.AddToRoleAsync(user, role);
    }
}