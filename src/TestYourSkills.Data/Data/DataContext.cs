﻿using Microsoft.EntityFrameworkCore;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Data.Data;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }
    
    public DbSet<Topic> Topics { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
}