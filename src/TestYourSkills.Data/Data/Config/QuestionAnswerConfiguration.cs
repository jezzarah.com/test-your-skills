﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Data.Data.Config;

public class QuestionAnswerConfiguration : IEntityTypeConfiguration<QuestionAnswer>
{
    public void Configure(EntityTypeBuilder<QuestionAnswer> builder)
    {
        builder.Property(qa => qa.Message).IsRequired().HasMaxLength(500);
    }
}