﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Data.Data.Config;

public class TopicConfiguration : IEntityTypeConfiguration<Topic>
{
    public void Configure(EntityTypeBuilder<Topic> builder)
    {
        builder.Property(t => t.Name).IsRequired().HasMaxLength(250);
    }
}