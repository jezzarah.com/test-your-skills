﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Data.Data.Config;

public class QuestionConfiguration : IEntityTypeConfiguration<Question>
{
    public void Configure(EntityTypeBuilder<Question> builder)
    {
        builder.Property(q => q.Title).IsRequired().HasMaxLength(250);
        builder.Property(q => q.Details).HasMaxLength(4000);
    }
}