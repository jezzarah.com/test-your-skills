﻿using System.Reflection;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using TestYourSkills.Core.Models.Questions;

namespace TestYourSkills.Data.Data;

public class DataContextSeed
{
    public static async Task SeedDefaultQuestionsAsync(DataContext context, IConfiguration configuration)
    {
        var assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        var seedDataPath = Path.Combine(assemblyPath!, "Data", "SeedData");
        
        if (!context.Topics.Any())
        {
            var topicData = await File.ReadAllTextAsync(Path.Combine(seedDataPath, "questions.json"));
            var topics = JsonSerializer.Deserialize<List<Topic>>(topicData);
            context.Topics.AddRange(topics!);
        }
        
        if (context.ChangeTracker.HasChanges()) await context.SaveChangesAsync();
    }
}